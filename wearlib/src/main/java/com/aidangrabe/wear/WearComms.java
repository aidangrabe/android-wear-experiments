package com.aidangrabe.wear;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.os.Handler;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.wearable.MessageApi;
import com.google.android.gms.wearable.MessageEvent;
import com.google.android.gms.wearable.Node;
import com.google.android.gms.wearable.NodeApi;
import com.google.android.gms.wearable.Wearable;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by aidan on 15/11/14.
 * Utility Singleton class for communicating over bluetooth using Android Wear
 */
public class WearComms implements GoogleApiClient.ConnectionCallbacks, MessageApi.MessageListener  {
    private static final String TAG_D = WearComms.class.getCanonicalName() + ".DEBUG";
    private static WearComms mInstance;
    private GoogleApiClient mGoogleApiClient;
    private Context mContext;

    // this map holds the response listeners to be called when a message has been sent, and we are
    // waiting for a response from the peer.
    private Map<String, WearResponse> mPendingComms;
    private Map<String, WearResponse> mListeners;

    // when we are not ready to start sending/receiving data, queue the messages in this map
    // and flushPendingSends() should be called when ready
    private Map<String, byte[]> mPendingSends;

    private Node mPeerNode;

    // setting up the GoogleApiClient and peernode may take a while, this is used to flag whether
    // or not communications can start
    private boolean isReady;

    private WearComms(Context context) {
        mGoogleApiClient = new GoogleApiClient.Builder(context)
                .addConnectionCallbacks(this)
                .addApi(Wearable.API)
                .build();
        mGoogleApiClient.connect();
        Wearable.MessageApi.addListener(mGoogleApiClient, this);

        mContext = context;

        mPendingComms = new HashMap<String, WearResponse>();
        mListeners = new HashMap<String, WearResponse>();
        mPendingSends = new HashMap<String, byte[]>();

        isReady = false;
    }

    /**
     * This method should be called before calling getInstance() to setup the GoogleApiClient
     * @param context should be the application context
     */
    public static void init(Context context) {
        if (mInstance == null) {
            mInstance = new WearComms(context);
        }
    }

    public static WearComms getInstance() {
        return mInstance;
    }

    /**
     * Add a listener for a given key/path
     * @param key the key to listen for
     * @param listener a listener that will be notified if/when a response is received on the same key
     * @return the WearComms instance for chaining
     */
    public WearComms addMessageListener(String key, WearResponse listener) {
        mListeners.put(key, listener);
        return this;
    }

    public WearResponse removeMessageListener(String key) {
        return mListeners.remove(key);
    }

    @Override
    public void onConnected(Bundle bundle) {
        getPeerNode();
    }

    @Override
    public void onConnectionSuspended(int i) {
        isReady = false;
    }

    @Override
    public void onMessageReceived(MessageEvent messageEvent) {
        final String path = messageEvent.getPath();
        final byte[] data = messageEvent.getData();
        Log.d(TAG_D, "Message received");
        WearResponse responseHandler = null;

        // check if this is a response to a previously sent message
        if (mPendingComms.containsKey(path)) {
            responseHandler = mPendingComms.get(path);
            mPendingComms.remove(path);
        }
        // if not, notify listeners of this particular path
        else {
            if (mListeners.containsKey(path)) {
                responseHandler = mListeners.get(path);
            }
        }

        // run the response handler on the main thread
        final WearResponse response = responseHandler;
        if (responseHandler != null) {
            Handler handler = new Handler(mContext.getMainLooper());
            handler.post(new Runnable() {
                @Override
                public void run() {
                    response.onResponse(path, data);
                }
            });
        }
    }

    /**
     * Send a string to the peer node with a response
     * @param key the path of the message to send
     * @param string the string to send
     * @param response the response object to call when a response for this key is received from
     *                 the peer
     */
    public void sendStringWithResponse(String key, String string, WearResponse response) {
        Log.d(TAG_D, "sendStringWithResponse()");
        sendString(key, string);
        mPendingComms.put(key, response);
    }

    /**
     * Send a string to the peer node
     * @param key the path of the message to send
     * @param string the string to send
     */
    public void sendString(String key, String string) {
        Log.d(TAG_D, "sendString()");
        sendBytes(key, string == null ? null : string.getBytes());
    }

    /**
     * Send some raw bytes to the peer node
     * @param key the path of the message to send
     * @param bytes the bytes to send
     */
    public void sendBytes(String key, byte[] bytes) {
        Log.d(TAG_D, "sendBytes()");
        if (!isReady) {
            mPendingSends.put(key, bytes);
        } else {
            PendingResult<MessageApi.SendMessageResult> pendingResult = Wearable.MessageApi
                    .sendMessage(mGoogleApiClient, mPeerNode.getId(), key, bytes);
        }
    }

    private void getPeerNode() {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                NodeApi.GetConnectedNodesResult nodes = Wearable.NodeApi.getConnectedNodes(mGoogleApiClient)
                        .await();

                for (final Node node : nodes.getNodes()) {
                    PendingResult<MessageApi.SendMessageResult> result = Wearable.MessageApi.sendMessage(mGoogleApiClient,
                            node.getId(), "/com.aidangrabe.wear.handshake", null);
                    result.setResultCallback(new ResultCallback<MessageApi.SendMessageResult>() {
                        @Override
                        public void onResult(MessageApi.SendMessageResult sendMessageResult) {
                            mPeerNode = node;
                            onReady();
                        }
                    });
                }

                return null;
            }
        }.execute();
    }

    private void onReady() {
        Log.d(TAG_D, "WearComms ready for communicating...");
        isReady = true;
        flushPendingSends();
    }

    private void flushPendingSends() {
        Log.d(TAG_D, "Flushing pending sends");
        for (String path : mPendingSends.keySet()) {
            sendBytes(path, mPendingSends.get(path));
        }
    }

    /**
     * Callback interface for when a message is received from the peer node
     */
    public interface WearResponse {
        public void onResponse(String key, byte[] data);
    }

}
