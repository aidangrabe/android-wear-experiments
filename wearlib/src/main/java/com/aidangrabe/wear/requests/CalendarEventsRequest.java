package com.aidangrabe.wear.requests;

/**
 * Created by aidan on 24/11/14.
 *
 */
public class CalendarEventsRequest extends JsonWearRequest {

    private int numHours;

    public CalendarEventsRequest(int numHours) {
        this.numHours = numHours;
    }

    public int getNumHours() {
        return numHours;
    }

}
