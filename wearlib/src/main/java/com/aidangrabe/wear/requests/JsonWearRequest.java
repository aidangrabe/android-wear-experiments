package com.aidangrabe.wear.requests;

import com.google.gson.Gson;

/**
 * Created by aidan on 24/11/14.
 *
 *
 */
public abstract class JsonWearRequest {

    public String toJson() {
        return new Gson().toJson(this);
    }

    @Override
    public String toString() {
        return toJson();
    }

}
