package com.aidangrabe.hellowatch;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.aidangrabe.bluetooth.BluetoothClient;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.wearable.MessageApi;
import com.google.android.gms.wearable.Node;
import com.google.android.gms.wearable.NodeApi;
import com.google.android.gms.wearable.Wearable;
import com.google.android.gms.wearable.WearableStatusCodes;

import java.util.List;

/**
 * Created by aidan on 23/01/15.
 * This Activity allows the user to send messages with Bluetooth and Android Wear and compare the
 * communication times
 */
public class BenchmarkActivity extends Activity implements ResultCallback<MessageApi.SendMessageResult>, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {


    private List<Node> mNodes;
    private GoogleApiClient mGClient;
    private BluetoothClient mBClient;
    private long mCommTimeStart;
    private long mCommTimeResult;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_benchmark);

        findViewById(R.id.button_wear).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendMessage(mGClient, "/benchmark", "test-string");
            }
        });

        mGClient = new GoogleApiClient.Builder(this).addApi(Wearable.API).addConnectionCallbacks(this).addOnConnectionFailedListener(this).build();
        mCommTimeStart = 0;
        mCommTimeResult = 0;

    }

    @Override
    protected void onResume() {
        super.onResume();

        mGClient.connect();

    }

    @Override
    protected void onPause() {
        super.onPause();

        mGClient.disconnect();

    }

    private void sendMessage(GoogleApiClient client, String path, String message) {
        mCommTimeStart = System.nanoTime();
        for (Node node : mNodes) {
            Wearable.MessageApi.sendMessage(client, node.getId(), path, message.getBytes()).setResultCallback(this);
        }
    }


    private void getConnectedNodes() {
        Wearable.NodeApi.getConnectedNodes(mGClient).setResultCallback(new ResultCallback<NodeApi.GetConnectedNodesResult>() {
            @Override
            public void onResult(NodeApi.GetConnectedNodesResult result) {
                mNodes = result.getNodes();
            }
        });
    }

    public void showResult(String commType) {
        Toast.makeText(this, commType + ": " + mCommTimeResult, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onResult(MessageApi.SendMessageResult sendMessageResult) {

        if (sendMessageResult.getStatus().getStatusCode() == WearableStatusCodes.SUCCESS) {
            mCommTimeResult = System.nanoTime() - mCommTimeStart;
            showResult("AndroidWear");
        } else {
            Log.d("D", "Message failed to send");
        }

    }

    @Override
    public void onConnected(Bundle bundle) {
        getConnectedNodes();
    }

    @Override
    public void onConnectionSuspended(int i) {
        Toast.makeText(this, "Connection Lost", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Toast.makeText(this, "Failed to connect", Toast.LENGTH_LONG).show();
        finish();
    }
}
