package com.aidangrabe.hellowatch;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.aidangrabe.hellowatch.lightsout.LightsOutActivity;
import com.aidangrabe.hellowatch.minesweeper.MineSweeperActivity;
import com.aidangrabe.hellowatch.sensors.AccelerometerActivity;
import com.aidangrabe.hellowatch.sensors.MessagePassing;
import com.aidangrabe.hellowatch.sensors.OrientationActivity;
import com.aidangrabe.hellowatch.sensors.RotationActivity;
import com.aidangrabe.hellowatch.todo.ToDoListActivity;
import com.aidangrabe.hellowatch.util.WearComms;
import com.aidangrabe.hellowatch.voice.VoiceActivity;
import com.aidangrabe.wear.requests.CalendarEventsRequest;

import java.util.HashMap;
import java.util.Map;

public class MainActivity extends Activity {

    public static final String DEBUG_TAG = "HELLO_WATCH";

    private ListView mListView;
    private Activity mActivity;
    private ArrayAdapter<String> mAdapter;
    private Map<String, Class<? extends Activity>> mScreens;

    private final AdapterView.OnItemClickListener mListItemClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            String key = (String) parent.getItemAtPosition(position);
            Class<? extends Activity> activityClass = mScreens.get(key);
            if (activityClass != null) {
                Intent activityIntent = new Intent(mActivity.getApplicationContext(), activityClass);
                mActivity.startActivity(activityIntent);
            } else {
                Toast.makeText(mActivity.getApplicationContext(), "Not yet implemented", Toast.LENGTH_SHORT).show();
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(DEBUG_TAG, "onCreate... started");

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        WearComms.init(getApplicationContext());
        WearComms.getInstance().sendString("/calendar", new CalendarEventsRequest(12).toJson());

        mActivity = this;
        mScreens = new HashMap<String, Class<? extends Activity>>();
        mScreens.put("Accelerometer", AccelerometerActivity.class);
        mScreens.put("Orientation", OrientationActivity.class);
        mScreens.put("Messages", MessagePassing.class);
        mScreens.put("Rotation Vector", RotationActivity.class);
        mScreens.put("Contacts", ContactsActivity.class);
        mScreens.put("Database", DatabaseActivity.class);
        mScreens.put("Custom List", CustomListActivity.class);
        mScreens.put("Basic Gestures", BasicGestureActivity.class);
        mScreens.put("Pattern Unlock", PatternUnlockActivity.class);
        mScreens.put("LightsOut!", LightsOutActivity.class);
        mScreens.put("Mine Sweeper", MineSweeperActivity.class);
        mScreens.put("ToDo List", ToDoListActivity.class);
        mScreens.put("Voice", VoiceActivity.class);
        mScreens.put("Bluetooth", BluetoothActivity.class);
        mScreens.put("Benchmark", BenchmarkActivity.class);
        setupList();
    }

    // initialize the list
    private void setupList() {
        mAdapter = new ArrayAdapter<String>(mActivity.getApplicationContext(), android.R.layout.simple_list_item_1);
        for (String title : mScreens.keySet()) {
            mAdapter.add(title);
        }
        mAdapter.notifyDataSetChanged();

        mListView = (ListView) findViewById(R.id.list_view);
        mListView.setAdapter(mAdapter);
        mListView.setOnItemClickListener(mListItemClickListener);
    }
}
