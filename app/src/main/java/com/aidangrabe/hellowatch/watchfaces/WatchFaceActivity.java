package com.aidangrabe.hellowatch.watchfaces;

import android.app.Activity;
import android.hardware.display.DisplayManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.Display;

import java.util.Calendar;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Basic watch face example.
 */
public abstract class WatchFaceActivity extends Activity implements TimeBroadcastReceiver.ClockListener {

    private DisplayManager mDisplayManager;
    private TimeBroadcastReceiver mBroadcastTimeReceiver;
    private boolean isDimmed;
    private Clock mClock;

    private final DisplayManager.DisplayListener mDisplayListener = new DisplayManager.DisplayListener() {
        @Override
        public void onDisplayAdded(int displayId) {

        }

        @Override
        public void onDisplayRemoved(int displayId) {
        }

        @Override
        public void onDisplayChanged(int displayId) {
            Display display = mDisplayManager.getDisplay(displayId);

            switch (display.getState()) {
                case Display.STATE_DOZE:    // when screen dimmed/un-dimmed
                    isDimmed = true;
                    onScreenDimmed();
                    break;
                case Display.STATE_OFF:     // when the user interacts with notifications
                    onScreenDimmed();
                    break;
                case Display.STATE_ON:      // when screen undimmed
                    isDimmed = false;
                    onScreenOn();
                    break;
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        isDimmed = false;
        mClock = new Clock(Looper.getMainLooper(), this);
        mClock.start();
        mDisplayManager = (DisplayManager) getSystemService(DISPLAY_SERVICE);
        mDisplayManager.registerDisplayListener(mDisplayListener, new Handler());
        mBroadcastTimeReceiver = new TimeBroadcastReceiver(this);
        mBroadcastTimeReceiver.register(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mDisplayManager.unregisterDisplayListener(mDisplayListener);
        mBroadcastTimeReceiver.unregister(this);
        mClock.stop();
    }

    @Override
    protected void onPause() {
        super.onPause();
        onScreenDimmed();
    }

    @Override
    protected void onResume() {
        super.onResume();
        onScreenOn();
    }

    public boolean isScreenDimmed() {
        return isDimmed;
    }

    protected void onScreenDimmed() {
        mClock.stop();
    }

    protected void onScreenOn() {
        mClock.start();
    }

}

