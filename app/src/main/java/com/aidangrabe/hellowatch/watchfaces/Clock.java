package com.aidangrabe.hellowatch.watchfaces;

import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;

import java.util.Calendar;

public class Clock {

    private Handler mHandler;
    private TimeBroadcastReceiver.ClockListener mListener;

    private Runnable mTickRunnable = new Runnable() {
        @Override
        public void run() {
            mListener.onTimeChanged(Calendar.getInstance());
            long now = SystemClock.uptimeMillis();
            long nextTick = now + (1000 - now % 1000);
            mHandler.postAtTime(this, nextTick);
        }
    };

    public Clock(Looper looper, TimeBroadcastReceiver.ClockListener listener) {
        mHandler = new Handler(looper);
        mListener = listener;
    }

    public void start() {
        stop();
        mHandler.post(mTickRunnable);
    }

    public void stop() {
        mHandler.removeCallbacksAndMessages(null);
    }

}
