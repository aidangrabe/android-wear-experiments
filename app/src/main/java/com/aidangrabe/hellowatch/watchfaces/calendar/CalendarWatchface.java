package com.aidangrabe.hellowatch.watchfaces.calendar;

import android.os.Bundle;
import android.support.v4.view.GestureDetectorCompat;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;

import com.aidangrabe.hellowatch.R;
import com.aidangrabe.hellowatch.util.WearComms;
import com.aidangrabe.hellowatch.watchfaces.WatchFaceActivity;
import com.aidangrabe.wear.CalendarEvent;
import com.aidangrabe.wear.requests.CalendarEventsRequest;
import com.google.gson.Gson;

import java.util.Calendar;

/**
 * Created by aidan on 12/11/14.
 *
 */
public class CalendarWatchface extends WatchFaceActivity {
    private static final String TAG_D = "WATCH_FACE";
    private static final int CALENDAR_LOOKAHEAD_HOURS = 11;
    private CalendarFace mCalendarFace;
    private boolean retrieveCalendar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        WearComms.init(getApplicationContext());

        setContentView(R.layout.activity_watchface_calendar);

        setupViews();

        retrieveCalendar = true;
        getEvents();
    }



    @Override
    public boolean onTouchEvent(MotionEvent event) {
        Log.d("GESTURE", "onTouchEvent");
        return super.onTouchEvent(event);
    }

    @Override
    public void onScreenDimmed() {
        super.onScreenDimmed();
        mCalendarFace.invalidate();
    }

    @Override
    public void onScreenOn() {
        super.onScreenOn();
    }

    private void setupViews() {
        mCalendarFace = (CalendarFace) findViewById(R.id.calendar_face);
        mCalendarFace.setSecondHandHidden(true);
        mCalendarFace.invalidate();
    }

    @Override
    public void onTimeChanged(Calendar calendar) {
        mCalendarFace.setTime(calendar);
        if (calendar.get(Calendar.MINUTE) == 0) {
            if (retrieveCalendar) {
                retrieveCalendar = false;
                getEvents();
            }
        } else {
            retrieveCalendar = true;
        }
    }

    private void getEvents() {
        WearComms.getInstance().sendStringWithResponse("/calendar", new CalendarEventsRequest(CALENDAR_LOOKAHEAD_HOURS).toJson(), new WearComms.WearResponse() {
            @Override
            public void onResponse(String key, byte[] data) {
                String json = new String(data);
                Log.d("JSON", json);
                mCalendarFace.clearArcs();
                CalendarEvent[] events = new Gson().fromJson(json, CalendarEvent[].class);
                for (CalendarEvent event : events) {
                    mCalendarFace.addColorArc(event.getStartTime(), event.getEndTime(), event.getColor());
                }
                if (events.length > 0) {
                    mCalendarFace.setNextEvent(events[0].getTitle(), events[0].getLocation());
                } else {
                    mCalendarFace.setNextEvent(null, null);
                }
            }
        });
    }
}
