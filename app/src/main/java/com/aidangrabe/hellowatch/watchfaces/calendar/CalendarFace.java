package com.aidangrabe.hellowatch.watchfaces.calendar;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

import com.aidangrabe.hellowatch.watchfaces.ClockHand;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by aidan on 13/11/14.
 *
 */
public class CalendarFace extends View {

    private Paint mPaint, mNumberPaint, mOutlinePaint;
    private float mRadius;
    private ClockHand mSecondHand, mMinuteHand, mHourHand;
    private boolean mSecondHandHidden;
    private ArrayList<ClockArc> mArcs;

    private String nextEventTitle, nextEventLocation;


    public CalendarFace(Context context, AttributeSet attrs) {
        super(context, attrs);
        mPaint = new Paint();
        mPaint.setColor(Color.RED);
        mPaint.setStrokeWidth(3);

        mOutlinePaint = new Paint();
        mOutlinePaint.setStyle(Paint.Style.STROKE);
        mOutlinePaint.setColor(Color.BLACK);
        mOutlinePaint.setTextAlign(Paint.Align.CENTER);

        mNumberPaint = new Paint();
        mNumberPaint.setColor(Color.WHITE);
        mNumberPaint.setTextAlign(Paint.Align.CENTER);

        mRadius = 100;
        mSecondHand = new ClockHand(getWidth() / 2, getHeight() / 2, mRadius);
        mMinuteHand = new ClockHand(getWidth() / 2, getHeight() / 2, mRadius);
        mHourHand = new ClockHand(getWidth() / 2, getHeight() / 2, mRadius);

        mSecondHandHidden = false;
        mArcs = new ArrayList<ClockArc>();
    }

    public void addColorArc(Date startTime, int endTime, int color) {
        mArcs.add(new ClockArc(startTime, endTime, color));
    }

    public void setTime(Calendar calendar) {
        int seconds = calendar.get(Calendar.SECOND);
        float degrees = seconds / 60.0f * 360;
        mSecondHand.setAngle(degrees);
        int minutes = calendar.get(Calendar.MINUTE);
        degrees = minutes / 60f * 360;
        mMinuteHand.setAngle(degrees);
        int hours = calendar.get(Calendar.HOUR);
        degrees = (hours * 60 + minutes) / 720f * 360;
        mHourHand.setAngle(degrees);
        invalidate();
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        mSecondHand.setCenterPosition(w / 2, h / 2);
        mSecondHand.setLength(w / 2 - 10);
        // minutes
        mMinuteHand.setCenterPosition(w / 2, h / 2);
        mMinuteHand.setLength(w / 2 - 10 - 20);
        // hours
        mHourHand.setCenterPosition(w / 2, h / 2);
        mHourHand.setLength(w / 4);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        for (ClockArc arc : mArcs) {
            arc.draw(canvas);
        }

        ClockHand.Position center = mSecondHand.getCenterPosition();
        ClockHand.Position tip = mSecondHand.getTipPosition();
        if (!mSecondHandHidden) {
            canvas.drawLine(center.x, center.y, tip.x, tip.y, mPaint);
        }

        drawNextEventDetails(canvas);

        center = mMinuteHand.getCenterPosition();
        tip = mMinuteHand.getTipPosition();
        canvas.drawLine(center.x, center.y, tip.x, tip.y, mPaint);

        center = mHourHand.getCenterPosition();
        tip = mHourHand.getTipPosition();
        canvas.drawLine(center.x, center.y, tip.x, tip.y, mPaint);

        drawNumbers(canvas);
    }

    public void drawNumbers(Canvas canvas) {
        ClockHand.Position center = mSecondHand.getCenterPosition();

        final float radius = getWidth() / 2 - 10;
        for (int i = 1; i <= 12; i++) {
            float xx, yy, angle;
            angle = (float) Math.toRadians(i / 12f * 360 - 90);
            xx = center.x + (float) (Math.cos(angle) * radius);
            yy = center.y + (float) (Math.sin(angle) * radius) - ((mNumberPaint.ascent() + mNumberPaint.descent()) / 2);
            canvas.drawText("" + i, xx, yy, mNumberPaint);
        }
    }

    private void drawNextEventDetails(Canvas canvas) {
        if (nextEventTitle == null) {
            return;
        }
        float x = canvas.getWidth() / 2;
        float y = canvas.getHeight() / 3;

        // Event title
        canvas.drawText(nextEventTitle, x, y - 10, mOutlinePaint);
        canvas.drawText(nextEventTitle, x, y - 10, mNumberPaint);

        // Event location
        canvas.drawText(nextEventLocation, x, y + 10, mOutlinePaint);
        canvas.drawText(nextEventLocation, x, y + 10, mNumberPaint);

    }

    public void setNextEvent(String title, String location) {
        nextEventTitle = title;
        nextEventLocation = location;
    }

    public void setSecondHandHidden(boolean hidden) {
        mSecondHandHidden = hidden;
    }

    public void clearArcs() {
        mArcs.clear();
    }

    private class ClockArc {
        private Paint paint;
        private float startAngle, sweepAngle;

        public ClockArc(Date startTime, int endTime, int color) {
            paint = new Paint();
            paint.setColor(color);
            paint.setStrokeWidth(8);
            paint.setStyle(Paint.Style.STROKE);
            paint.setStrokeCap(Paint.Cap.ROUND);
            paint.setStrokeJoin(Paint.Join.ROUND);
            paint.setAntiAlias(true);

            Calendar calendar = Calendar.getInstance();
            calendar.setTime(startTime);
            startAngle = (calendar.get(Calendar.HOUR) * 60 + calendar.get(Calendar.MINUTE)) / 720f * 360;

            sweepAngle = (endTime / 60f) / 720f * 360;
        }

        public void draw(Canvas canvas) {
            // minus 90 because degrees start at 3 o clock
            drawArc(canvas, getWidth() / 2 - 20, startAngle - 90, sweepAngle, paint);
        }

        private void drawArc(Canvas canvas, float radius, float startAngle, float sweepAngle, Paint paint) {
            int x, y, sx, sy;
            boolean hasStarted = false;
            Path path = new Path();
            sx = getWidth() / 2;
            sy = getHeight() / 2;
            for (int i = 0; i < sweepAngle; i++) {
                float angle = startAngle + i;
                x = sx + (int) (Math.cos(Math.toRadians(angle)) * radius);
                y = sy + (int) (Math.sin(Math.toRadians(angle)) * radius);
                if (hasStarted) {
                    path.lineTo(x, y);
                } else {
                    hasStarted = true;
                    path.moveTo(x, y);
                }
            }
            canvas.drawPath(path, paint);
        }
    }
}
