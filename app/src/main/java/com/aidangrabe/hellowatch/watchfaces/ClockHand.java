package com.aidangrabe.hellowatch.watchfaces;

/**
 * Created by aidan on 13/11/14.
 *
 */
public class ClockHand {

    private Position mPosition, mTipPosition;
    private float mAngle, mLength;

    public ClockHand(float centerX, float centerY, float length) {
        mPosition = new Position(centerX, centerY);
        mTipPosition = new Position(centerX, centerY);
        mLength = length;

        setAngle(0);
    }

    public Position getCenterPosition() {
        return mPosition;
    }

    public Position getTipPosition() {
        return mTipPosition;
    }

    public void setAngle(float degrees) {
        float x, y;
        double rad;
        mAngle = degrees - 90;
        rad = Math.toRadians(mAngle);

        x = mPosition.x + (float) (Math.cos(rad) * mLength);
        y = mPosition.y + (float) (Math.sin(rad) * mLength);

        mTipPosition = new Position(x, y);
    }

    public void setCenterPosition(float x, float y) {
        mPosition = new Position(x, y);
    }

    public void setLength(float length) {
        mLength = length;
    }

    public class Position {
        public final float x, y;

        public Position(float x, float y) {
            this.x = x;
            this.y = y;
        }

        @Override
        public String toString() {
            return String.format("(%f, %f)", x, y);
        }

    }

}
