package com.aidangrabe.hellowatch.lightsout;

/**
 * Created by aidan on 30/10/14.
 *
 */
public class LevelManager {

    private int[][] levels;
    private int currentLevel;

    public LevelManager() {
        levels = new int[][] {
                {0, 1, 0, 0,
                 0, 0, 0, 0,
                 0, 0, 0, 0,
                 0, 0, 1, 0},

                {0, 1, 1, 0,
                 0, 0, 0, 0,
                 0, 0, 0, 0,
                 0, 1, 1, 0},

                {1, 0, 0, 1,
                 0, 0, 0, 0,
                 0, 0, 0, 0,
                 1, 0, 0, 1},

                {0, 1, 0, 0,
                 0, 0, 0, 1,
                 1, 0, 0, 0,
                 0, 0, 1, 0}
        };
        currentLevel = 0;
    }

    public int[] nextLevel() {
        if (currentLevel + 1 > levels.length) {
            return new int[0];
        }
        return levels[currentLevel++];
    }

    public int getOptimalScore(int[] level) {
        int score = 0;
        for (int i = 0; i < level.length; i++) {
            if (level[i] == 1) {
                score++;
            }
        }
        return score;
    }

    public int getScore(int[] level, int playerMoves) {
        if (playerMoves <= 0) return 0;

        int optimalScore = getOptimalScore(level);
        return (int) (optimalScore / (float) playerMoves * 100);
    }

    public int[] getCurrentLevel() {
        return levels[currentLevel - 1];
    }

}
