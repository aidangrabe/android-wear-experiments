package com.aidangrabe.hellowatch.lightsout;

import android.os.Bundle;
import android.support.v4.util.Pair;
import android.util.Log;
import android.view.WindowManager;
import android.widget.Toast;

import com.aidangrabe.hellowatch.base.GridActivity;

/**
 * Created by aidan on 21/10/14.
 *
 */
public class LightsOutActivity extends GridActivity {

    private LevelManager mLevelManager;
    private int mPlayerMoves;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        mCols = 4;
        mRows = 4;
        mPlayerMoves = 0;
        mLevelManager = new LevelManager();

        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        applyLevel(mLevelManager.nextLevel());
    }

    @Override
    protected void onButtonCreated(GridButton button) {
        button.setText("O");
    }

    @Override
    protected void onButtonClicked(GridButton button) {
        selectButton(button);
        mPlayerMoves++;
        if (checkWin()) {
            int score = mLevelManager.getScore(mLevelManager.getCurrentLevel(), mPlayerMoves);
            Toast.makeText(getApplicationContext(), "Next Level - Score " + score, Toast.LENGTH_LONG).show();
            mPlayerMoves = 0;

            int[] level = mLevelManager.nextLevel();
            if (level.length > 0) {
                applyLevel(level);
            } else {
                Toast.makeText(getApplicationContext(), "You Win - Score " + score, Toast.LENGTH_LONG).show();
            }
        }
    }

    public boolean checkWin() {
        boolean isWin = true;
        search:
        for (int y = 0; y < mRows; y++) {
            for (int x = 0; x < mCols; x++) {
                if (!mGrid[x][y].getText().toString().equalsIgnoreCase("O")) {
                    isWin = false;
                    break search;
                }
            }
        }
        return isWin;
    }

    public void selectButton(GridButton button) {
        Pair<Integer, Integer> position = button.getPosition();
        int x = position.first;
        int y = position.second;

        for (int i = -1; i < 2; i+=2) {
            toggleButton(getButton(x + i, y));
            toggleButton(getButton(x, y + i));
        }
    }

    public void toggleButton(GridButton button) {
        if (button == null) {
            return;
        }

        if (button.getText().toString().equalsIgnoreCase("x")) {
            button.setText("O");
        } else {
            button.setText("X");
        }
    }

    public void applyLevel(int[] level) {
        for (int i=0; i<level.length; i++) {
            if (level[i] == 0) continue;

            int x, y;
            x = i % mCols;
            y = i / mCols;
            Log.d("LIGHTS_OUT", "Pressing button: " + x + " , " + y);
            selectButton(getButton(x, y));
        }
    }

}
