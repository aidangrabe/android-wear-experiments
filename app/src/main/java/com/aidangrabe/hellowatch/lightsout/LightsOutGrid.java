package com.aidangrabe.hellowatch.lightsout;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.util.Pair;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import com.aidangrabe.hellowatch.MainActivity;

/**
 * Created by aidan on 21/10/14.
 *
 */
public class LightsOutGrid {

    private int cols, rows;
    private Context context;
    private LightButton[][] grid;
    private View.OnClickListener lightClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            LightButton lightButton = (LightButton) v;
            Pair<Integer, Integer> position = lightButton.getPosition();
            int x = position.first;
            int y = position.second;

            for (int i = -1; i < 2; i+=2) {
                int j = x + i;
                if (j > -1 && j < cols) {
                    grid[j][y].toggle();
                }
                j = y + i;
                if (j > -1 && j < rows) {
                    grid[x][j].toggle();
                }
            }
            Log.d(MainActivity.DEBUG_TAG, "Clicked button: " +  x + ", " + ++y);
        }
    };

    public LightsOutGrid(Context context, int cols, int rows) {
        this.cols = cols;
        this.rows = rows;
        this.context = context;
        grid = new LightButton[cols][rows];
    }

    public void setupView(LinearLayout rootView) {
        LightButton button;

        for (int y = 0; y < rows; y++) {
            LinearLayout.LayoutParams rowParams = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.MATCH_PARENT, 1.0f);
            LinearLayout rowLayout = new LinearLayout(context);
            rowLayout.setOrientation(LinearLayout.HORIZONTAL);
            rowLayout.setLayoutParams(rowParams);
            for (int x = 0; x < cols; x++) {
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.MATCH_PARENT, 1.0f);

                button = new LightButton(x, y);
                button.setOnClickListener(lightClickListener);
                button.setText("X");
                button.setLayoutParams(params);

                grid[x][y] = button;
                rowLayout.addView(button);
            }
            rootView.addView(rowLayout);
        }
    }

    private class LightButton extends Button {

        private int x, y;
        private boolean isLit;
        private static final int LIT_COLOR = Color.RED;
        private static final int DARK_COLOR = Color.GREEN;

        public LightButton(int x, int y) {
            super(context);
            this.x = x;
            this.y = y;
            setIsLit(false);
        }

        public boolean isLit() {
            return isLit;
        }

        public void setIsLit(boolean isLit) {
            this.isLit = isLit;
            setBackgroundColor(isLit ? LIT_COLOR : DARK_COLOR);
            setText(isLit ? "O" : "X");
        }

        public void toggle() {
            setIsLit(!isLit);     // toggle the light
        }

        public Pair<Integer, Integer> getPosition() {
            return new Pair<Integer, Integer>(x, y);
        }

    }

}
