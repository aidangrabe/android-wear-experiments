package com.aidangrabe.hellowatch;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

/**
 * Created by aidan on 17/10/14.
 * Activity with custom list
 */
public class CustomListActivity extends Activity {

    private Context mContext;
    private ListView mListView;
    private ArrayAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        mListView = (ListView) findViewById(R.id.list_view);
        mContext = this;
        mAdapter = new CustomAdapter();
        mAdapter.add(new CustomListItem("Sad Cloud", R.drawable.ic_full_sad));
        mAdapter.add(new CustomListItem("Icon", R.drawable.ic_launcher));
        mAdapter.add(new CustomListItem("+1", R.drawable.ic_plusone_medium_off_client));
        mAdapter.add(new CustomListItem("Cloud Cancel", R.drawable.ic_full_cancel));
        mAdapter.add(new CustomListItem("Sign In", R.drawable.common_signin_btn_icon_dark));
        mAdapter.add(new CustomListItem("Close Button", R.drawable.close_button));
        mAdapter.notifyDataSetChanged();
        mListView.setAdapter(mAdapter);
    }

    class CustomListItem {
        public String title;
        public int imageId;
        public CustomListItem(String title, int imageId) {
            this.title = title;
            this.imageId = imageId;
        }
    }

    class CustomAdapter extends ArrayAdapter<CustomListItem> {

        public CustomAdapter() {
            super(mContext, R.layout.list_custom_item);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View listItemView = convertView;

            if (listItemView== null) {
                listItemView = inflater.inflate(R.layout.list_custom_item, parent, false);
            }
            CustomListItem listItem = getItem(position);

            ((TextView) listItemView.findViewById(R.id.text_view)).setText(listItem.title);
            ((ImageView) listItemView.findViewById(R.id.image_view)).setImageDrawable(getResources().getDrawable(listItem.imageId));

            return listItemView;

        }
    }

}
