package com.aidangrabe.hellowatch;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.wearable.MessageApi;
import com.google.android.gms.wearable.MessageEvent;
import com.google.android.gms.wearable.Node;
import com.google.android.gms.wearable.NodeApi;
import com.google.android.gms.wearable.Wearable;

import org.json.JSONArray;
import org.json.JSONException;

/**
 * Created by aidan on 10/10/14.
 *
 */
public class ContactsActivity extends Activity implements GoogleApiClient.ConnectionCallbacks, MessageApi.MessageListener {

    private GoogleApiClient mGoogleApiClient;
    private Node mPeer;
    private ListView mListView;
    private ArrayAdapter<String> mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_contacts);

        setupApiClient();
        askForContacts();

        mAdapter = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_list_item_1);
        mListView = (ListView) findViewById(R.id.list_view);
        mListView.setAdapter(mAdapter);

    }

    private void setupApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addApi(Wearable.API)
                .build();
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnected(Bundle bundle) {
        Log.v(MainActivity.DEBUG_TAG, "Wearable connected to Google Play Services");
        Wearable.MessageApi.addListener(mGoogleApiClient, this);
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    protected void onStop() {
        super.onStop();
        Wearable.MessageApi.removeListener(mGoogleApiClient, this);
    }

    @Override
    public void onMessageReceived(MessageEvent messageEvent) {
        byte[] bytes = messageEvent.getData();

        String data = bytes.length > 0 ? new String(bytes) : "[No Data]";
        Log.v(MainActivity.DEBUG_TAG, "Received message " + messageEvent.getPath() + " ====>>>> " + data);

        if (messageEvent.getPath().endsWith("contacts") && bytes.length > 0) {
            final String jsonString = new String(bytes);
            Handler mainHandler = new Handler(getApplicationContext().getMainLooper());

            Runnable myRunnable = new Runnable() {
                @Override
                public void run() {
                    JSONArray json = null;
                    try {
                        json = new JSONArray(jsonString);
                        for (int i = 0; i < json.length(); i++) {
                            mAdapter.add(json.getString(i));
                        }
                        mAdapter.notifyDataSetChanged();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            };
            mainHandler.post(myRunnable);
        }
    }

    private void askForContacts() {
        getPeerNode();
    }

    private void getPeerNode() {

        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                NodeApi.GetConnectedNodesResult nodes = Wearable.NodeApi.getConnectedNodes(mGoogleApiClient)
                        .await();

                for (final Node node : nodes.getNodes()) {
                    PendingResult<MessageApi.SendMessageResult> result = Wearable.MessageApi.sendMessage(mGoogleApiClient,
                            node.getId(), "/contacts", null);
                    result.setResultCallback(new ResultCallback<MessageApi.SendMessageResult>() {
                        @Override
                        public void onResult(MessageApi.SendMessageResult sendMessageResult) {
                            mPeer = node;
                        }
                    });
                }

                return null;
            }
        }.execute();

    }

}
