package com.aidangrabe.hellowatch.sensors;

import android.hardware.Sensor;
import android.os.Bundle;
import android.util.Log;

import com.aidangrabe.hellowatch.MainActivity;

import java.util.List;

public class OrientationActivity extends XYZSensorActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        getIntent().putExtra(XYZSensorActivity.ARG_TYPE, Sensor.TYPE_MAGNETIC_FIELD);
        super.onCreate(savedInstanceState);

        List<Sensor> deviceSensors = mSensorManager.getSensorList(Sensor.TYPE_ALL);
        for (Sensor s : deviceSensors) {
            Log.d(MainActivity.DEBUG_TAG, s.getName());
        }

    }

}
