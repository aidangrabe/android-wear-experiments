package com.aidangrabe.hellowatch.sensors;

import android.hardware.Sensor;
import android.os.Bundle;

/**
 * Created by aidan on 06/10/14.
 *
 */
public class RotationActivity extends XYZSensorActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        getIntent().putExtra(XYZSensorActivity.ARG_TYPE, Sensor.TYPE_ROTATION_VECTOR);

        super.onCreate(savedInstanceState);

    }
}
