package com.aidangrabe.hellowatch.sensors;

import android.hardware.Sensor;
import android.os.Bundle;

public class AccelerometerActivity extends XYZSensorActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getIntent().putExtra(XYZSensorActivity.ARG_TYPE, Sensor.TYPE_LINEAR_ACCELERATION);

        super.onCreate(savedInstanceState);
    }
}
