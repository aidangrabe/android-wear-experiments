package com.aidangrabe.hellowatch.sensors;

import android.app.Activity;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.aidangrabe.hellowatch.R;

import java.security.InvalidParameterException;
import java.util.InvalidPropertiesFormatException;

public abstract class XYZSensorActivity extends Activity implements SensorEventListener {

    public static final String ARG_TYPE = "sensor_type";

    protected SensorManager mSensorManager;
    protected Sensor mAccelerometer;
    protected float[] mLastValues;
    protected TextView[] mLabels;

    protected int mType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_xyz);

        // check for required sensor type argument in the extras
        Bundle extras = getIntent().getExtras();
        if (extras == null || !extras.containsKey(ARG_TYPE)) {
            throw new InvalidParameterException("ARG_TYPE must be provided");
        }
        mType = extras.getInt(ARG_TYPE);

        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        mAccelerometer = mSensorManager.getDefaultSensor(mType);

        mLabels = new TextView[3];

        LinearLayout root = (LinearLayout) findViewById(R.id.main);

        // create + add the x,y,z labels
        for (int i = 0; i < 3; i++) {
            TextView tv = new TextView(this.getApplicationContext());

            root.addView(tv, new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT));
            mLabels[i] = tv;
        }

        mLastValues = new float[3];
        for (int i = 0; i < mLastValues.length; i++) {
            mLastValues[i] = 0;
        }

    }

    @Override
    public void onSensorChanged(SensorEvent event) {

        Sensor sensor = event.sensor;
        if (sensor.getType() == mType) {

            mLabels[0].setText("x: " + event.values[0]);
            mLabels[1].setText("y: " + event.values[1]);
            mLabels[2].setText("z: " + event.values[2]);

            // copy the event values into out mLastValues array
            mLastValues = event.values.clone();
        }

    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mAccelerometer != null) {
            mSensorManager.registerListener(this, mAccelerometer, SensorManager.SENSOR_DELAY_NORMAL);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        mSensorManager.unregisterListener(this);
    }
}
