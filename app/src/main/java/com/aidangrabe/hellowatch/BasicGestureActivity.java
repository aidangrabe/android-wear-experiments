package com.aidangrabe.hellowatch;

import android.app.Activity;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.view.WindowManager;
import android.widget.TextView;

import java.util.Collections;

/**
 * Created by aidan on 17/10/14.
 *
 */
public class BasicGestureActivity extends Activity implements SensorEventListener {

    private SensorManager mSensorManager;
    private Sensor mAccelerometer;

    private TextView mGestureTypeLabel;
    private TextView mLabelX;
    private TextView mLabelY;
    private TextView mLabelZ;

    private double mLastGestureTime;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_basic_gesture);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        mAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION);

        mGestureTypeLabel = (TextView) findViewById(R.id.gesture_type);
        mLabelX = (TextView) findViewById(R.id.accel_x);
        mLabelY = (TextView) findViewById(R.id.accel_y);
        mLabelZ = (TextView) findViewById(R.id.accel_z);

        mLastGestureTime = 0;
    }

    @Override
    protected void onPause() {
        super.onPause();

        mSensorManager.unregisterListener(this, mAccelerometer);
    }

    @Override
    protected void onResume() {
        super.onResume();

        mSensorManager.registerListener(this, mAccelerometer, SensorManager.SENSOR_DELAY_FASTEST);
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        if (event.sensor.getType() == Sensor.TYPE_LINEAR_ACCELERATION) {
            detectGesture(event.values[0], event.values[1], event.values[2]);
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
    }

    public void detectGesture(double x, double y, double z) {
        final double threshold = 10;
        final int gestureTimeThreshold = 1000;  // 1 second

        double absX = Math.abs(x);
        double absY = Math.abs(y);
        double absZ = Math.abs(z);
        double biggest = Math.max(absX, absY);
        biggest = Math.max(absZ, biggest);

        if (biggest < threshold || System.currentTimeMillis() - mLastGestureTime < gestureTimeThreshold) {
            return;
        }

        mLastGestureTime = System.currentTimeMillis();

        String gestureLessThan     = "LEFT";
        String gestureGreaterThan  = "RIGHT";
        double val = x;

        if (absY == biggest) {
            gestureLessThan     = "DOWN";
            gestureGreaterThan  = "UP";
            val = y;
        } else if (absZ == biggest) {
            gestureLessThan     = "FORWARDS";
            gestureGreaterThan  = "BACKWARDS";
            val = z;
        }
        String gesture = val > 0 ? gestureGreaterThan : gestureLessThan;

        mLabelX.setText("x: " + x);
        mLabelY.setText("y: " + y);
        mLabelZ.setText("z: " + z);
        mGestureTypeLabel.setText(gesture);

    }

}
