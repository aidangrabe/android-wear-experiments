package com.aidangrabe.hellowatch.todo.createtodo;

import java.util.ArrayList;

/**
 * Created by aidan on 08/11/14.
 *
 */
public class NewToDoListModule extends NewToDoList {

    public NewToDoListModule() {
        mListItems = new ArrayList<NewToDoListItem>();
        mListItems.add(new NewToDoListItem("Networking", null));
        mListItems.add(new NewToDoListItem("Software Engineering", null));
        mListItems.add(new NewToDoListItem("Databases", null));
    }

}
