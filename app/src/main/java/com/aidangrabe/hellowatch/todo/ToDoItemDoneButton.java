package com.aidangrabe.hellowatch.todo;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.Button;

/**
 * Created by aidan on 08/11/14.
 *
 */
public class ToDoItemDoneButton extends Button {

    private int mRow;

    public ToDoItemDoneButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        mRow = 0;
    }

    public void setRow(int row) {
        mRow = row;
    }

    public int getRow() {
        return mRow;
    }

}
