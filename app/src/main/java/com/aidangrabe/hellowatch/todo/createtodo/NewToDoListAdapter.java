package com.aidangrabe.hellowatch.todo.createtodo;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

/**
 * Created by aidan on 08/11/14.
 *
 */
public class NewToDoListAdapter extends ArrayAdapter<NewToDoListItem> {

    public NewToDoListAdapter(Context context, int resource) {
        super(context, resource);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = super.getView(position, convertView, parent);

        NewToDoListItem item = getItem(position);
        TextView tv = (TextView) view.findViewById(android.R.id.text1);
        tv.setText(item.getTitle());

        return view;
    }
}
