package com.aidangrabe.hellowatch.todo;

import android.app.Activity;
import android.database.Cursor;
import android.os.Bundle;
import android.support.wearable.view.GridPagerAdapter;
import android.support.wearable.view.GridViewPager;
import android.util.Log;

import com.aidangrabe.hellowatch.R;
import com.aidangrabe.hellowatch.todo.createtodo.NewToDoListAdapter;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by aidan on 01/11/14.
 * Simple to-do list activity
 */
public class ToDoListActivity extends Activity {

    private GridViewPager mGridViewPager;
    private GridPagerAdapter mAdapter;
    private NewToDoListAdapter mNewListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_todo);

        ToDoItemManager.init(getApplicationContext());
        mNewListAdapter = new NewToDoListAdapter(getApplicationContext(), android.R.layout.simple_list_item_1);

        mGridViewPager = (GridViewPager) findViewById(R.id.grid_view_pager);
        mAdapter = new ToDoGridPagerAdapter(getApplicationContext(), mNewListAdapter);

        mGridViewPager.setAdapter(mAdapter);

    }

}
