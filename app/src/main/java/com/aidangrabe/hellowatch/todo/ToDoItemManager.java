package com.aidangrabe.hellowatch.todo;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by aidan on 04/11/14.
 * Singleton class to handle creating, retrieving and storing ToDoItems
 */
public class ToDoItemManager extends SQLiteOpenHelper {

    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "ToDoList.db";
    private static ToDoItemManager mItemManager;

    public static abstract class ToDoEntry {
        public static final String TABLE_NAME = "items";
        public static final String COL_ID = "id";
        public static final String COL_TITLE = "title";
        public static final String COL_CREATED_ON = "created_on";
    }

    public static abstract class Sql {
        public static final String CREATE_TABLE =
                "CREATE TABLE " + ToDoEntry.TABLE_NAME + " (" +
                        ToDoEntry.COL_ID + " INTEGER PRIMARY KEY," +
                        ToDoEntry.COL_TITLE + " VARCHAR(255)," +
                        ToDoEntry.COL_CREATED_ON + " INTEGER" +
                        ")";
        private static final String DROP_TABLE =
                "DROP TABLE IF EXISTS " + ToDoEntry.TABLE_NAME;
    }

    private ToDoItemManager(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public static void init(Context context) {
        if (mItemManager == null) {
            mItemManager = new ToDoItemManager(context);
        }
    }

    public static ToDoItemManager getInstance() {
        // could throw exception here if mItemManager is null if init(context) was not called
        return mItemManager;
    }

    public Cursor getAll() {
        SQLiteDatabase db = getReadableDatabase();

        String[] projection = {
                ToDoEntry.COL_ID,
                ToDoEntry.COL_TITLE,
                ToDoEntry.COL_CREATED_ON
        };
        String orderBy = ToDoEntry.COL_CREATED_ON + " DESC";

        return db.query(ToDoEntry.TABLE_NAME, projection, null, null, null, null, orderBy);
    }

    private ContentValues makeContentValues(ToDoItem item) {
        ContentValues cv = new ContentValues();
        cv.put(ToDoEntry.COL_TITLE, item.getTitle());
        cv.put(ToDoEntry.COL_CREATED_ON, item.getCreationDate().toString());
        return cv;
    }

    public void delete(ToDoItem item) {
        if (item.getId() == -1) {
            // if the item has no id, it has not yet been written to the db
            return;
        }
        SQLiteDatabase db = getReadableDatabase();
        db.delete(ToDoEntry.TABLE_NAME, ToDoEntry.COL_ID + " = ?", new String[] {"" + item.getId()});
    }

    public void update(ToDoItem item) {
        String whereClaus = String.format("%s = ?", ToDoEntry.COL_ID);
        String[] whereArgs = new String[] {"" + item.getId()};
        SQLiteDatabase db = getWritableDatabase();
        db.update(ToDoEntry.TABLE_NAME, makeContentValues(item), whereClaus, whereArgs);
    }

    public void save(ToDoItem item) {
        SQLiteDatabase db = getWritableDatabase();
        db.insert(ToDoEntry.TABLE_NAME, "", makeContentValues(item));
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(Sql.CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(Sql.CREATE_TABLE);
        onCreate(db);
    }
}
