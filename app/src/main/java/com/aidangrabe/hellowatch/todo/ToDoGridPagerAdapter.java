package com.aidangrabe.hellowatch.todo;

import android.content.Context;
import android.database.Cursor;
import android.support.wearable.view.GridPagerAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.aidangrabe.hellowatch.R;
import com.aidangrabe.hellowatch.todo.createtodo.NewToDoList;
import com.aidangrabe.hellowatch.todo.createtodo.NewToDoListAdapter;
import com.aidangrabe.hellowatch.todo.createtodo.NewToDoListItem;
import com.aidangrabe.hellowatch.todo.createtodo.NewToDoListType;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by aidan on 01/11/14.
 *
 */
public class ToDoGridPagerAdapter extends GridPagerAdapter {

    private Context mContext;
    private ArrayList<ToDoItem> mTodoItems;
    private Button mNewTodoButton;
    private NewToDoListAdapter mListAdapter;
    private ListView mListView;
    private String newToDoListText;

    private View.OnClickListener mNewTodoOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            newToDoListText = "";
            mNewTodoButton.setVisibility(View.GONE);
            setupList();
            mListView.setVisibility(View.VISIBLE);
            mListAdapter.notifyDataSetChanged();
        }
    };

    private AdapterView.OnItemClickListener mListItemClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            NewToDoListItem item = mListAdapter.getItem(position);
            newToDoListText += item.getTitle() + " ";
            if (item.hasNextList()) {
                setupList(item.getNextList());
            } else {
                ToDoItem todoItem = new ToDoItem(newToDoListText, new Date());
                ToDoItemManager.getInstance().save(todoItem);
                refresh();
            }
        }
    };

    private View.OnClickListener mDoneButtonOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            // delete the item from the db
            ToDoItemDoneButton button = (ToDoItemDoneButton) v;
            ToDoItem item = mTodoItems.get(button.getRow() - 1);
            if (item != null) {
                ToDoItemManager.getInstance().delete(item);
                refresh();
            }
        }
    };

    public ToDoGridPagerAdapter(Context context, NewToDoListAdapter listAdapter) {
        mContext = context;
        mTodoItems = new ArrayList<ToDoItem>();
        mListAdapter = listAdapter;

        refresh();
        newToDoListText = "";
        setupList();
    }

    public void refresh() {
        ToDoItemManager itemManager = ToDoItemManager.getInstance();
        Cursor c = itemManager.getAll();
        mTodoItems.clear();
        while (c.moveToNext()) {
            mTodoItems.add(ToDoItem.instanceFromCursor(c));
        }
        notifyDataSetChanged();
        Log.d("DEBUG", String.format("Number of items after refresh(): %d", mTodoItems.size()));
    }

    private void setupList() {
        setupList(new NewToDoListType());
    }

    private void setupList(NewToDoList list) {
        mListAdapter.clear();
        mListAdapter.addAll(list.getItems());
        mListAdapter.notifyDataSetChanged();
        Log.d("TODO", String.format("Num items: %d", list.getItems().size()));
    }

    @Override
    public int getColumnCount(int arg0) {
        return 1;
    }

    @Override
    public int getRowCount() {
        return mTodoItems.size() + 1;
    }

    @Override
    protected Object instantiateItem(ViewGroup container, int row, int col) {
        View view;

        if (row == 0) {
            view = LayoutInflater.from(mContext).inflate(R.layout.new_todo_item, container, false);
            mNewTodoButton = (Button) view.findViewById(R.id.new_todo_buton);
            mNewTodoButton.setOnClickListener(mNewTodoOnClickListener);

            ListView lv = (ListView) view.findViewById(R.id.list_view);
            lv.setVisibility(View.GONE);
            lv.setAdapter(mListAdapter);
            lv.setOnItemClickListener(mListItemClickListener);
            mListView = lv;

            container.addView(view);
        } else {
            view = LayoutInflater.from(mContext).inflate(R.layout.grid_view_pager_item, container, false);
            final TextView textView = (TextView) view.findViewById(R.id.textView);
            final ToDoItemDoneButton button = (ToDoItemDoneButton) view.findViewById(R.id.done_button);
            button.setRow(row);
            button.setOnClickListener(mDoneButtonOnClickListener);

            final ToDoItem item = mTodoItems.get(row - 1);
            textView.setText(String.format("%s", item.getTitle()));
            container.addView(view);
        }
        return view;
    }

    @Override
    protected void destroyItem(ViewGroup container, int row, int col, Object view) {
        container.removeView((View)view);
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view==object;
    }

}
