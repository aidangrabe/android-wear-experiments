package com.aidangrabe.hellowatch.todo.createtodo;

import java.util.ArrayList;

/**
 * Created by aidan on 08/11/14.
 *
 */
public class NewToDoListType extends NewToDoList {

    public NewToDoListType() {
        mListItems = new ArrayList<NewToDoListItem>();
        mListItems.add(new NewToDoListItem("Assignment", new NewToDoListModule()));
        mListItems.add(new NewToDoListItem("Lab", null));
        mListItems.add(new NewToDoListItem("Homework", null));
        mListItems.add(new NewToDoListItem("Meeting", null));
    }

}
