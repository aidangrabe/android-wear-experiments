package com.aidangrabe.hellowatch.todo;

import android.database.Cursor;

import com.google.gson.Gson;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by aidan on 04/11/14.
 *
 */
public class ToDoItem {

    private int id;

    private String title;
    private String description;
    private Date creationDate;
    private Date completionDate;

    private ToDoItem() {}

    public ToDoItem(String title, Date creationDate) {
        this.id = -1;
        this.title = title;
        this.creationDate = creationDate;
    }

    public ToDoItem(int id, String title, Date creationDate) {
        this(title, creationDate);
        this.id = id;
    }

    // create a To-Do list item from the current position of a Cursor object
    public static ToDoItem instanceFromCursor(Cursor c) {
        String title = c.getString(c.getColumnIndexOrThrow(ToDoItemManager.ToDoEntry.COL_TITLE));
        int id = c.getInt(c.getColumnIndexOrThrow(ToDoItemManager.ToDoEntry.COL_ID));
        String dateString = c.getString(c.getColumnIndexOrThrow(ToDoItemManager.ToDoEntry.COL_CREATED_ON));

        Date date = new Date();
        try {
            DateFormat df = new SimpleDateFormat("EEE MMM dd kk:mm:ss z yyyy", Locale.ENGLISH);
            date = df.parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return new ToDoItem(id, title, date);
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String toString() {
        Gson json = new Gson();
        return json.toJson(this);
    }
}
