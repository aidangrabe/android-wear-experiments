package com.aidangrabe.hellowatch.todo.createtodo;

/**
 * Created by aidan on 08/11/14.
 *
 */
public class NewToDoListItem {

    protected NewToDoList mNextList;
    protected String mTitle;

    public NewToDoListItem(String title, NewToDoList nextList) {
        mTitle = title;
        mNextList = nextList;
    }

    public NewToDoList getNextList() {
        return mNextList;
    }

    public boolean hasNextList() {
        return mNextList != null;
    }

    public String getTitle() {
        return mTitle;
    }

}
