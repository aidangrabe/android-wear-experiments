package com.aidangrabe.hellowatch.todo.createtodo;

import java.util.ArrayList;

/**
 * Created by aidan on 08/11/14.
 *
 */
public abstract class NewToDoList {

    protected ArrayList<NewToDoListItem> mListItems;

    public NewToDoList() {

    }

    public NewToDoList(ArrayList<NewToDoListItem> items) {
        mListItems = items;
    }

    public ArrayList<NewToDoListItem> getItems() {
        return mListItems;
    }

}
