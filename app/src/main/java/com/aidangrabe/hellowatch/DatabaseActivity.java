package com.aidangrabe.hellowatch;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.wearable.MessageApi;
import com.google.android.gms.wearable.MessageEvent;
import com.google.android.gms.wearable.Node;
import com.google.android.gms.wearable.NodeApi;
import com.google.android.gms.wearable.Wearable;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by aidan on 11/10/14.
 *
 */
public class DatabaseActivity extends Activity implements GoogleApiClient.ConnectionCallbacks, MessageApi.MessageListener {
    private GoogleApiClient mGoogleApiClient;
    private TextView mTextView;

    private View.OnClickListener mDownloadButtonListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Log.d(MainActivity.DEBUG_TAG, "Initiating download sequence...");
            mTextView.setText("Downloading...");
            getDataFromPhone();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_database);

        findViewById(R.id.download_button).setOnClickListener(mDownloadButtonListener);
        mTextView = (TextView) findViewById(R.id.text_view);

        setupApiClient();

    }

    private void getDataFromPhone() {

        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                NodeApi.GetConnectedNodesResult nodes = Wearable.NodeApi.getConnectedNodes(mGoogleApiClient)
                        .await();

                for (final Node node : nodes.getNodes()) {
                    PendingResult<MessageApi.SendMessageResult> result = Wearable.MessageApi.sendMessage(mGoogleApiClient,
                            node.getId(), "/web", null);
                    result.setResultCallback(new ResultCallback<MessageApi.SendMessageResult>() {
                        @Override
                        public void onResult(MessageApi.SendMessageResult sendMessageResult) {

                        }
                    });
                }

                return null;
            }
        }.execute();

    }

    private void setupApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addApi(Wearable.API)
                .build();
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnected(Bundle bundle) {
        Wearable.MessageApi.addListener(mGoogleApiClient, this);
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onMessageReceived(MessageEvent messageEvent) {
        if (messageEvent.getPath().endsWith("web")) {
            Log.d(MainActivity.DEBUG_TAG, "Received web information from phone");
            if (messageEvent.getData().length == 0) {
                Log.d(MainActivity.DEBUG_TAG, "web information was empty though :(");
                return;
            }

            try {
                final JSONObject json = new JSONObject(new String(messageEvent.getData()));
                Log.d(MainActivity.DEBUG_TAG, json.toString());
                Handler handler = new Handler(getApplicationContext().getMainLooper());
                Runnable runnable = new Runnable() {
                    @Override
                    public void run() {
                        mTextView.setText(json.toString());
                    }
                };
                handler.post(runnable);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        Wearable.MessageApi.removeListener(mGoogleApiClient, this);
    }
}
