package com.aidangrabe.hellowatch;


import android.app.Activity;
import android.os.Bundle;
import android.widget.Toast;

import com.asghonim.salp.OnPasswordCompleteListener;
import com.asghonim.salp.PasswordGrid;

/**
 * Created by aidan on 20/10/14.
 * Testing pattern unlock
 */
public class PatternUnlockActivity extends Activity implements OnPasswordCompleteListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pattern_unlock);
        ((PasswordGrid)findViewById(R.id.password_grid)).setListener(this);
    }

    @Override
    public void onPasswordComplete(String s) {
        Toast.makeText(this, "Password: " + s, Toast.LENGTH_SHORT).show();
    }
}
