package com.aidangrabe.hellowatch;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.Set;
import java.util.UUID;

/**
 * Created by aidan on 18/01/15.
 *
 */
public class BluetoothActivity extends Activity {

    public static final String SERVICE_UUID = "4b6e3550-9f63-11e4-bcd8-0800200c9a66";

    private BluetoothAdapter mBtAdapter;
    private BluetoothSocket mServer;
    private BluetoothDevice mServerDevice;
    private BufferedReader mInReader;
    private PrintWriter mOutWriter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mBtAdapter = BluetoothAdapter.getDefaultAdapter();
        if (mBtAdapter == null) {
            Toast.makeText(this, "No Bluetooth", Toast.LENGTH_SHORT).show();
            finish();
            return;
        }

        setContentView(R.layout.activity_bluetooth);

        ((Button) findViewById(R.id.bluetooth_button)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                write("This is my message!");
            }
        });

        discoverServerDevice();

    }

    @Override
    protected void onResume() {
        super.onResume();

        connect();
        write("HELLO");

    }

    @Override
    protected void onPause() {
        super.onPause();

        disconnect();

    }

    private void connect() {
        Log.d("D", "Connecting");
        try {
            mServer = mServerDevice.createInsecureRfcommSocketToServiceRecord(UUID.fromString(SERVICE_UUID));
            mServer.connect();
            mInReader = new BufferedReader(new InputStreamReader(mServer.getInputStream()));
            mOutWriter = new PrintWriter(mServer.getOutputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void disconnect() {

        try {
            mServer.close();
            mInReader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        mOutWriter.close();

    }

    private void discoverServerDevice() {

        Set<BluetoothDevice> devices = mBtAdapter.getBondedDevices();
        for (BluetoothDevice device : devices) {
            mServerDevice = device;
            Log.d("D", "ServerDevice: " + device.getName());
        }

    }

    private void write(String msg) {

        Log.d("D", "Writing message: " + msg);
        mOutWriter.println(msg);
        mOutWriter.flush();

    }

}
