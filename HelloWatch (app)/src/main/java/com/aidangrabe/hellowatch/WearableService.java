package com.aidangrabe.hellowatch;

import android.database.Cursor;
import android.net.Uri;
import android.provider.CalendarContract;
import android.provider.ContactsContract;
import android.util.Log;

import com.aidangrabe.wear.CalendarEvent;
import com.aidangrabe.wear.WearComms;
import com.aidangrabe.wear.requests.CalendarEventsRequest;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.wearable.MessageApi;
import com.google.android.gms.wearable.MessageEvent;
import com.google.android.gms.wearable.Wearable;
import com.google.android.gms.wearable.WearableListenerService;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by aidan on 04/10/14.
 *
 */
public class WearableService extends WearableListenerService {

    private GoogleApiClient mGoogleApiClient;
    private String mPeerId;

    private Response.Listener<JSONObject> mJsonListener = new Response.Listener<JSONObject>() {
        @Override
        public void onResponse(JSONObject response) {
            sendMessage("web", response.toString());
        }
    };

    private Response.ErrorListener mJsonErrorListener = new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
            Log.d(MainActivity.APP_TAG, "An error occurred downloading data");
        }
    };

    @Override
    public void onCreate() {
        super.onCreate();
        WearComms.init(getApplicationContext());
        WearComms.getInstance().addMessageListener("/calendar", new WearComms.WearResponse() {
            @Override
            public void onResponse(String key, byte[] data) {
                Log.d("JSON", "received: " + new String(data));
                CalendarEventsRequest request = new Gson().fromJson(new String(data), CalendarEventsRequest.class);
                Log.d("JSON", "request: " + request);
                String json = new Gson().toJson(getCalendarEvents(request.getNumHours()));
                Log.d("JSON", json);
                WearComms.getInstance().sendString(key, json);
            }
        });
    }

    @Override
    public void onMessageReceived(MessageEvent messageEvent) {
        super.onMessageReceived(messageEvent);

        WearComms.getInstance().onMessageReceived(messageEvent);

        mPeerId = messageEvent.getSourceNodeId();

        if (messageEvent.getPath().endsWith("contacts")) {
            Log.d(MainActivity.APP_TAG, "Getting contacts");
            String[] contacts = getContacts();
            sendMessage("/contacts", contacts);
        } else if (messageEvent.getPath().endsWith("web")) {
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, "http://www.aidangrabe.com/wear/test-json.json",
                    null, mJsonListener, mJsonErrorListener);

            MyVolley.init(getApplicationContext());
            MyVolley.getRequestQueue().add(request);
        }

    }

    // @param numHours get events for this many hours
    private ArrayList<CalendarEvent> getCalendarEvents(int numHours) {
        Cursor calendar = getContentResolver().query(CalendarContract.CalendarEntity.CONTENT_URI, null, null, null, null);
        Set<String> calendarIds = new HashSet<String>();
        while (calendar.moveToNext()) {
            String calendarName = calendar.getString(calendar.getColumnIndex(
                    CalendarContract.CalendarEntity.CALENDAR_DISPLAY_NAME));
            String calendarId = calendar.getString(calendar.getColumnIndex(
                    CalendarContract.CalendarEntity._ID));
            calendarIds.add(calendarId);
        }
        calendar.close();

        long currentTime = System.currentTimeMillis();

        String where = null;
        String[] whereArgs = new String[] {};
        String sortOrder = String.format("%s ASC", CalendarContract.Instances.DTSTART);
        ArrayList<CalendarEvent> events = new ArrayList<CalendarEvent>();

//        Uri instancesUri = CalendarContract.Instances.CONTENT_URI;
        Uri[] uris = new Uri[] {CalendarContract.Instances.CONTENT_URI};//,
//            CalendarContract.Events.CONTENT_URI};

        for (Uri uri : uris) {
            uri = uri.buildUpon()
                    .appendPath(Long.toString(currentTime))
                    .appendPath(Long.toString(currentTime + 1000 * 60 * 60 * numHours))
                    .build();
            Cursor instances = getContentResolver().query(uri, null, where, whereArgs, sortOrder);
            while (instances.moveToNext()) {
                events.add(CalendarEvent.instanceFromCursor(instances));
            }
            instances.close();
        }

        return events;
    }

    private String[] getContacts() {
        Cursor contacts = getContentResolver().query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);
        String aNameFromContacts[] = new String[contacts.getCount()];
        int i = 0;

        while(contacts.moveToNext()) {
            String contactName = contacts.getString(contacts.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
            aNameFromContacts[i] = contactName;
            i++;
        }

        contacts.close();
        return aNameFromContacts;
    }

    private void sendMessage(String path, String[] data) {
        String[] dts = new String[10];
        for (int i = 0; i < 5; i++) {
            dts[i] = data[i];
        }

        final JSONArray json = new JSONArray(Arrays.asList(dts));
        String jsonString = json.toString();

        sendMessage(path, jsonString);
    }


    private void sendMessage(String path, String message) {
        PendingResult<MessageApi.SendMessageResult> result = Wearable.MessageApi.sendMessage(getGoogleApiClient(),
                mPeerId, path, message.getBytes());
        result.setResultCallback(new ResultCallback<MessageApi.SendMessageResult>() {
            @Override
            public void onResult(MessageApi.SendMessageResult sendMessageResult) {
                Log.v(MainActivity.APP_TAG, "Sent message to watch");
            }
        });
    }

    private GoogleApiClient getGoogleApiClient() {
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addApi(Wearable.API)
                    .build();
            mGoogleApiClient.connect();
        }
        return mGoogleApiClient;
    }
}
