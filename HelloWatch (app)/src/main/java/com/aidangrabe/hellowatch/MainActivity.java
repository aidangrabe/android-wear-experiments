package com.aidangrabe.hellowatch;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.ParcelUuid;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.nio.Buffer;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.UUID;


public class MainActivity extends Activity implements View.OnClickListener {

    public static final String APP_TAG = "HelloWatch::App";
    public static final String SERVICE_NAME = "BluetoothService";
    public static final String SERVICE_UUID = "4b6e3550-9f63-11e4-bcd8-0800200c9a66";

    private static volatile int clientId = 0;

    private Button mOnButton, mListDevicesButton;
    private BluetoothAdapter mBtAdapter;
    private BluetoothServerSocket mServerSocket;
    private List<BluetoothDevice> mDevices;
    private ServerHandler mServerHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mDevices = new ArrayList<>();

        mOnButton = (Button) findViewById(R.id.button_on);
        mListDevicesButton = (Button) findViewById(R.id.button_list_devices);

        mOnButton.setOnClickListener(this);
        mListDevicesButton.setOnClickListener(this);

        mBtAdapter = BluetoothAdapter.getDefaultAdapter();
        if (mBtAdapter == null) {
            Toast.makeText(this, "Bluetooth not available", Toast.LENGTH_SHORT).show();
            finish();
            return;
        }

        try {
            mServerSocket = mBtAdapter.listenUsingInsecureRfcommWithServiceRecord(SERVICE_NAME, UUID.fromString(SERVICE_UUID));
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Override
    protected void onResume() {
        super.onResume();

        mServerHandler = new ServerHandler();
        new Thread(mServerHandler).start();

    }

    @Override
    protected void onPause() {
        super.onPause();

        mServerHandler.stop();
        try {
            mServerSocket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

    }

    @Override
    public void onClick(View v) {

        if (mOnButton.equals(v)) {
            bluetoothTurnOn();
        } else if (mListDevicesButton.equals(v)) {
            bluetoothListPairedDevices();
        }

    }

    private void bluetoothTurnOn() {

        if (!mBtAdapter.isEnabled()) {
            Intent intent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(intent, 0);
            Log.d("D", "Turning on bluetooth");
        } else {
            Log.d("D", "Bluetooth already on");
        }

    }

    private void bluetoothListPairedDevices() {

        Set<BluetoothDevice> devices = mBtAdapter.getBondedDevices();
        List<String> deviceNames = new ArrayList<>();
        for (BluetoothDevice device : devices) {
            mDevices.add(device);
            for (ParcelUuid uuid : device.getUuids()) {
                Log.d("D", "UUID: " + uuid.getUuid().toString());
            }
            deviceNames.add(device.getName());
            Log.d("D", "Device found: " + device.getName());
        }

    }

    private class ServerHandler implements Runnable {
        private boolean running;

        public ServerHandler() {
            Log.d("D", "New Server Handler");
            running = true;
        }

        @Override
        public void run() {
            Log.d("D", "Server running");
            while(running) {
                Log.d("D", "run loop");
                try {
                    BluetoothSocket client = mServerSocket.accept();
                    new Thread(new ClientHandler(client)).start();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        public void stop() {
            running = false;
        }

    }

    private class ClientHandler implements Runnable {

        private int id;
        private BluetoothSocket mSocket;
        private PrintWriter mOutWriter;
        private BufferedReader mInReader;
        private boolean running;

        public ClientHandler(BluetoothSocket socket) {
            Log.d("D", "New ClientHandler");
            mSocket = socket;
            running = true;
            id = clientId++;

            try {
                mInReader = new BufferedReader(new InputStreamReader(mSocket.getInputStream()));
                mOutWriter = new PrintWriter(mSocket.getOutputStream(), true);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void run() {

            while (running) {
                String line = null;
                try {
                    line = mInReader.readLine();
                    if (line == null) {
                        running = false;
                        break;
                    }

                    final String msg = line;
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(MainActivity.this, String.format("[%d]: %s", id, msg), Toast.LENGTH_SHORT).show();
                        }
                    });

                    if (line.equals("HELLO")) {
                        mOutWriter.println("HI!");
                        mOutWriter.flush();
                        Log.d("D", "Server replied to 'HELLO'");
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    running = false;
                    break;
                }
                Log.d("D", "Server received line [id=" + id + "]: " + line);
            }
            Log.d("D", "Closed Client");
            disconnect();

        }

        public void disconnect() {

            try {
                mSocket.close();
                mInReader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            mOutWriter.close();

        }

        public void setRunning(boolean running) {
            this.running = running;
        }

    }

}
