package com.aidangrabe.hellowatch;

import android.content.Context;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

/**
 * Created by aidan on 11/10/14.
 * Simple wrapper class for a Volley RequestQueue
 */
public class MyVolley {

    private static RequestQueue mRequestQueue;

    private MyVolley() {}

    public static void init(Context context) {
        mRequestQueue = Volley.newRequestQueue(context);
    }

    public static RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            throw new IllegalStateException("Volley Queue was empty");
        }
        return mRequestQueue;
    }

}
