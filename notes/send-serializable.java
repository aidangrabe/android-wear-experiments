private void putData(List<ObjStationData> results) {
        if (results != null && results.size() > 0) {
            PutDataMapRequest putDataMapRequest = PutDataMapRequest.create(Constants.GET_NEAREST_DART);
            DataMap dataMap = putDataMapRequest.getDataMap();
 
            StationDataResponse objStationDataListSerializable = new StationDataResponse();
            objStationDataListSerializable.setObjStationDataList(results);
            byte[] data = SerializationUtils.serialize(objStationDataListSerializable);
            dataMap.putByteArray(Constants.STATION_DATA_LIST, data);
            dataMap.putString(Constants.STATION_NAME, mClosestStationName);
 
 
            Wearable.DataApi.putDataItem(mGoogleApiClient, putDataMapRequest.asPutDataRequest()).setResultCallback(new ResultCallback<DataApi.DataItemResult>() {
                @Override
                public void onResult(DataApi.DataItemResult dataItemResult) {
 
                }
            });
        } else {
            sendMessage(Constants.NO_DARTS_AVAILABLE, mClosestStationName);
        }
 
        stopSelf();
    }

// receive

@Override
    public void onDataChanged(DataEventBuffer dataEvents) {
        if (BuildConfig.DEBUG)
            Log.d(Constants.LOGTAG, "Data Changed: " + dataEvents.getCount());
 
        NotificationManager notificationManagerCompat = ((NotificationManager) getSystemService(NOTIFICATION_SERVICE));
        for (DataEvent dataEvent : dataEvents) {
            if (dataEvent.getType() == DataEvent.TYPE_CHANGED) {
                LocalBroadcastManager.getInstance(this).sendBroadcast(new Intent(WearConstants.ACTION_TRAIN_INFORMATION_RECEIVED));
                DataMap dataMap = DataMap.fromByteArray(dataEvent.getDataItem().getData());
                StationDataResponse objStationDataListSerializable = SerializationUtils.deserialize((byte[]) dataMap.get(Constants.STATION_DATA_LIST));
            }
        }
    }
